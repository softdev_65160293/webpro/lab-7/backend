import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersReposiroty: Repository<User>,
  ) {}

  create(createUserDto: CreateUserDto): Promise<User> {
    return this.usersReposiroty.save(createUserDto);
  }

  findAll(): Promise<User[]> {
    return this.usersReposiroty.find();
  }

  findOne(id: number) {
    return this.usersReposiroty.findOneBy({ id: id });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    await this.usersReposiroty.update(id, updateUserDto);
    const user = await this.usersReposiroty.findOneBy({ id });
    return user;
  }

  async remove(id: number) {
    const delUser = await this.usersReposiroty.findOneBy({ id });
    return this.usersReposiroty.remove(delUser);
  }
}
